import irm.option_funcs as of
import numpy as nm
from irm.swapmarket import SwaptionMarket
from scipy.linalg import eigh, cholesky
from scipy.stats import norm


'''
============================================================
HW3# 5. Generate and test correlated random numbers
============================================================
'''
def testCorrGaussRnNums():
    cor = 0.5
    cov = nm.matrix([[1, cor],[cor, 1]])
    randomNums = genCorrGaussRnNums(5e7, cov, 'cholesky')
    #now reverse engineer the covvariance matrix
    derivedCov = nm.cov(randomNums)
    epsilon = 0.05
    if(abs(derivedCov[0, 1] - cor) < epsilon):
        print("Correlated random numbers works well")
    else:
        print("Correlated random numbers does not work well")
    


'''
 inputs - number of random samples, 2/2 covariance matrix, and decompostion method (cholesky or eigen)
'''
def genCorrGaussRnNums(num_rnd_samples, cov_matrix, dec_method):
    # Generate samples from two independent normally distributed random
    # variables (with mean 0 and std. dev. 1).
    x = norm.rvs(size=(2, num_rnd_samples))

    # We need a matrix `c` for which `c*c^T = r`.  We can use, for example,
    # the Cholesky decomposition, or the we can construct `c` from the
    # eigenvectors and eigenvalues.

    if cov_matrix.ndim != 2:
        raise Exception("cov matrix dimension is not equal to 2!")    

    if dec_method == 'cholesky':
        # Compute the Cholesky decomposition.
        c = cholesky(cov_matrix, lower=True)
    else:
        # Compute the eigenvalues and eigenvectors.
        evals, evecs = eigh(cov_matrix)
        # Construct c, so c*c^T = r.
        c = nm.dot(evecs, nm.diag(nm.sqrt(evals)))

    # Convert the data to correlated random variables. 
    y = nm.dot(c, x)
        
    return y

'''
==================================================================
HW3# 6. Code to Generates the MC paths.
==================================================================
'''

'''
Calculates the SABR option prices using Monte-Carlo.

i_sabr_params: the SABR params as a 2d array as presented in the HW Handout
i_put_call: 'put' or 'call' depending on what we are valuing
i_num_sims: The number of simulations to run
i_dec_method: The decomposition method, 'cholesky' or 'eigen'.
 


'''
def CalculateSabrOptionPricesMC(i_sabr_params, i_put_call, i_num_sims, i_dec_method, i_num_time_steps):
    nrows=i_sabr_params.shape[0]
    prices =  [None] * (nrows)
    for i in range(0,nrows):
        print('Calculating ' + i_put_call + "# " + str(i) + "\n")
        #The covariance matrix
        r = nm.array([
             [1, i_sabr_params[i,6]],
             [i_sabr_params[i,6], 1]    
        ])
        fwds = SimulateFwdPrices(i_num_sims,r,i_dec_method,i_sabr_params[i,0],i_num_time_steps,i_sabr_params[i,1],i_sabr_params[i,3],i_sabr_params[i,4],i_sabr_params[i,5])
        prices[i] = CalculateOptionPrice(nm.matrix(fwds),i_sabr_params[i,2],i_put_call)  
    return prices


'''
#calculate option price

i_fwd_prices: The forward prices
i_strike: The forward prices
i_put_call: Indcates whether we are using a put or a call.    
'''
def CalculateOptionPrice(i_fwd_prices,i_strike,i_put_call):            
    if i_put_call == 'call':
        price=nm.average(nm.maximum((i_fwd_prices-i_strike),0))
    else:
        price=nm.average(nm.maximum((i_strike-i_fwd_prices),0))
    return price        

'''
    Evolve the forward rate.
    i_corr_rnd_samples: The correlated random variables.
    i_time: The time (in years)
    i_num_time_steps: The number of timesteps
    i_f0: Initial price
    i_sigma0: Initial vol (SABR param)
    i_alpha: alpha (SABR param)
    i_beta: beta (SABR param)
''' 
def evolveFwd(i_corr_rnd_samples, i_time, i_num_time_steps, i_f0, i_sigma0, i_alpha, i_beta):
    deltaT=i_time/i_num_time_steps
    sqrt_delta=nm.abs(nm.sqrt(deltaT))
    fwd =  [None] * (i_num_time_steps+1)
    sigma = [None] * (i_num_time_steps+1)
    fwd[0]=i_f0
    sigma[0]=i_sigma0
    for i in range(0, i_num_time_steps):
        #print(fwd[i])
        fwd[i+1] = max((fwd[i] + sigma[i]*(nm.power(fwd[i],i_beta))*sqrt_delta*i_corr_rnd_samples[0][i]),0)
        sigma[i+1] = sigma[i] * nm.exp(i_alpha*sqrt_delta*i_corr_rnd_samples[1][i] - (i_alpha*i_alpha*deltaT/2))
    return fwd[i_num_time_steps-1]

'''
calcualte terminal fwd price for N random paths
'''
def SimulateFwdPrices(i_num_paths, i_cov_matrix, i_method,i_time,i_num_time_steps,i_f0,i_sigma0,i_alpha,i_beta):
    fwd =  [None] * (i_num_paths)
    for i in range(0, i_num_paths):
        rnd_nums = genCorrGaussRnNums(i_num_time_steps+1, i_cov_matrix, i_method)
        #print(rnd_nums)
        fwd[i] = evolveFwd(rnd_nums, i_time, i_num_time_steps, i_f0, i_sigma0, i_alpha, i_beta)
        #print(fwd[i])    
    return fwd


def writeToCSV(obj, fullyQualifiedFileName):
    nm.savetxt(fullyQualifiedFileName, obj, delimiter=",")
    

if __name__ == '__main__':
    epsilon = 10**-8
    
    '''
    ============================================================
    HW3#1 implementing the Normal and LogNormal swaption models.
    ============================================================
    '''
    #Check Black's model (Lognormal) by making sure that PCP holds ATM
    atmBlacksCall = of.blacks_call(1, 1, 1, 0.2, 1)
    atmBlacksPut = of.blacks_put(1, 1, 1, 0.2, 1)
    if nm.absolute(atmBlacksCall - atmBlacksPut) > epsilon:
        raise Exception("ATM PCP does not hold for Blacks model")
    else:
        print("ATM PCP for Blacks model holds")
    
    #Test Put-Call parity of the normal model as a sanity check.
    atmNormalCall = of.normal_call(1, 1, 1, 0.2, 1)
    atmNormalPut = of.normal_put(1, 1, 1, 0.2, 1)
    if nm.absolute(atmNormalCall - atmNormalPut) > epsilon:
        raise Exception("ATM PCP does not hold for Normal model")
    else:
        print("ATM PCP for Normal model holds")
    
    
    
    ''''Build a swap market out of the excel'''
    maturities = nm.matrix([0.25, .5, 1, 2, 3, 5]).T
    fowradSwapRates = nm.matrix([3.510, 3.632, 3.872, 4.313, 4.657, 5.03]).T
    moneyness = nm.matrix([-250, -200, -150, -100, -50, -25, 0.00, 25, 50, 100, 150, 200, 250]).T
    lognormalVols = nm.matrix([
                        [0.4868776044,0.4170073060,0.3758977768,0.3534962835,0.3442718610,0.3431218514,0.3435808776,0.3452180973,0.3476887415,0.3541405025,0.3615503235,0.3691991111,0.3767322609],
                        [0.4539760008,0.3974819584,0.3628905988,0.3425116600,0.3320458504,0.3294890133,0.3282494355,0.3280359594,0.3286064531,0.3313575252,0.3353904350,0.3400453399,0.3449439178],
                        [0.4110175023,0.3671137865,0.3383383319,0.3199176401,0.3090205765,0.3057064352,0.3035022285,0.3022050613,0.3016387697,0.3021258197,0.3040531866,0.3068231897,0.3100573459],
                        [0.3630794746,0.3304871753,0.3073736657,0.2914311515,0.2811245681,0.2776788688,0.2751713369,0.2734625227,0.2724241891,0.2719128267,0.2728863761,0.2748056862,0.2773026544],
                        [0.3304251775,0.3026288522,0.2817582675,0.2665020066,0.2559626471,0.2522089185,0.2493290459,0.2472190641,0.2457767005,0.2445116757,0.2448499416,0.2462590545,0.2483537510],
                        [0.2969228412,0.2733096733,0.2545090655,0.2398825425,0.2290237602,0.2248891967,0.2215447128,0.2189261397,0.2169629323,0.2147045879,0.2141850553,0.2148909218,0.2164188674]
                        ])
    
    '''
    =====================================================================
    Code inside the SwaptionMarket performs HW3 #2,#3,#4 
    ======================================================================
    '''
    
    sm = SwaptionMarket(maturities, moneyness, fowradSwapRates, lognormalVols, 0.5)
    
    print('\nT = ')
    print(maturities)
    
    print('\n[sigma0, alpha, beta, rho] =')
    params = nm.append(sm.sigma0,sm.alpha,axis=1)
    params = nm.append(params,sm.beta,axis=1)
    params = nm.append(params,sm.rho,axis=1)
    print(params)
    
    print('\nMoneyness (bp) = ')
    print(moneyness.T)
    
    print('\nNormal Vols =')
    print(sm.normalVols)
    
    print('\nPct Error =')
    nrows = sm.lognormalVols.shape[0]
    calibrationErrors = nm.zeros((0, lognormalVols.shape[1]))
    for i in range(0, nrows):
        errorsForExpiry = 100*(sm.normalVols[i,:] - sm.sabrNormVols(sm.sigma0[i,0], sm.alpha[i,0], sm.beta[i,0], sm.rho[i,0], i).T)/sm.normalVols[i,:]
        calibrationErrors = nm.append(calibrationErrors, errorsForExpiry, axis = 0)
        print(errorsForExpiry)
    
    writeToCSV(calibrationErrors / 100.00, "./HW3Num4calibrationErrors.csv")
    
    ''''Part Two, pricing test options'''
    sabrParamsCalls = nm.matrix([
                        [0.25, 0.005, 0.01, 0.0072, 1.1, 0.2, 0.8],
                        [0.25, 0.05,  0.06, 0.0224, 0.8, 0.5, -0.2],
                        [0.5,  0.01,  0.02, 0.04,   0.8, 0.5, 0.7],
                        [0.5,  0.05,  0.06, 0.0182, 0.7, 0.2, -0.3],
                        [1,    0.015, 0.02, 0.0232, 0.6, 0.2, 0.4],
                        [1,    0.04,  0.05, 0.05,   0.5, 0.5, -0.3],
                        [2,    0.02,  0.03, 0.0707, 0.5, 0.5, 0.3],
                        [2,    0.06,  0.07, 0.0176, 0.5, 0.2, -0.1],
                        [5,    0.05,  0.06, 0.0146, 0.4, 0.2, 0.2],
                        [5,    0.05,  0.07, 0.0447, 0.3, 0.5, -0.5]
                        ])
    
    sabrParamsPuts = nm.matrix([
                        [0.25,  0.005,  0.002, 0.0354, 0.9,  0.5, 0.8],
                        [0.25,  0.04,   0.02,  0.0095, 0.9,  0.2, 0],
                        [0.5,   0.01,   0.005, 0.04,   0.7,  0.5, 0.4],
                        [0.5,   0.03,   0.02,  0.0577, 0.6,  0.5, -0.2],
                        [1,     0.03,   0.01,  0.0202, 0.6,  0.2, 0.5],
                        [1,     0.05,   0.04,  0.0182, 0.6,  0.2, -0.5],
                        [2,     0.04,   0.025, 0.05,   0.5,  0.5, 0.2],
                        [2,     0.05,   0.035, 0.0447, 0.5,  0.5, -0.1],
                        [5,     0.05,   0.035, 0.0146, 0.4,  0.2, 0.1],
                        [5,     0.06,   0.04,  0.0176, 0.4,  0.2, 0]
                        ])
    
    print("Print analytical call values")
    
    analyticalCalls = nm.matrix(nm.zeros((sabrParamsCalls.shape[0], 1)))
    for i in range(0, sabrParamsCalls.shape[0]):
        T = sabrParamsCalls[i, 0]
        f0 = sabrParamsCalls[i, 1]
        k = sabrParamsCalls[i, 2]
        sig0 = sabrParamsCalls[i, 3]
        alpha = sabrParamsCalls[i, 4]
        beta = sabrParamsCalls[i, 5]
        rho = sabrParamsCalls[i, 6]
        price = of.sabr_normal_call(1, f0, k, sig0, alpha, beta, rho, T)
        analyticalCalls[i, 0] = price
        print("normal call price for T = " + str(T) + " f0 = " + str(f0) + " k = " + str(k) + " sig0 = " + str(sig0) + " alpha = " + str(alpha) + " beta = " + str(beta) + " rho = " + str(rho) + " price = " + str(price)) 
    
    print("Print analytical put values")
    
    analyticalPuts = nm.matrix(nm.zeros((sabrParamsCalls.shape[0], 1)))
    for i in range(0, sabrParamsPuts.shape[0]):
        T = sabrParamsPuts[i, 0]
        f0 = sabrParamsPuts[i, 1]
        k = sabrParamsPuts[i, 2]
        sig0 = sabrParamsPuts[i, 3]
        alpha = sabrParamsPuts[i, 4]
        beta = sabrParamsPuts[i, 5]
        rho = sabrParamsPuts[i, 6]
        price = of.sabr_normal_put(1, f0, k, sig0, alpha, beta, rho, T)
        analyticalPuts[i, 0] = price
        print("normal put price for T = " + str(T) + " f0 = " + str(f0) + " k = " + str(k) + " sig0 = " + str(sig0) + " alpha = " + str(alpha) + " beta = " + str(beta) + " rho = " + str(rho) + " price = " + str(price))
        
    
    print("Testing correlated random number generator")
    testCorrGaussRnNums()
    
    '''
    ==================================================================
    HW3 #7. Code to calculate the SABR Option prices using monte carlo
    ==================================================================
    '''
    paths = int(1e5)
    timeSteps = int(1e2 * 3)
    
    callPricesMC = nm.matrix(CalculateSabrOptionPricesMC(sabrParamsCalls,'call', paths, 'cholesky', timeSteps)).T
    putPricesMC = nm.matrix(CalculateSabrOptionPricesMC(sabrParamsPuts,'put', paths, 'cholesky', timeSteps)).T
    
    print('Call prices Monte Carlo:')    
    print(callPricesMC)
    print('Put prices Monte Carlo:')
    print(putPricesMC)
    
    
    anaResults = nm.append(analyticalCalls, analyticalPuts, axis=0)
    mcResults = nm.append(callPricesMC, putPricesMC, axis=0)
    anaVsMCDiff = anaResults - mcResults
    
    '''Building the matrix to be written out'''
    anaVsMCResults = nm.append(sabrParamsCalls, sabrParamsPuts, axis=0)
    anaVsMCResults = nm.append(anaVsMCResults, anaResults, axis=1)
    anaVsMCResults = nm.append(anaVsMCResults, mcResults, axis=1)
    anaVsMCResults = nm.append(anaVsMCResults, anaVsMCDiff, axis=1)
    writeToCSV(anaVsMCResults, './anaVsMCResults.csv')

