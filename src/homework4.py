import irm.option_funcs as of
import numpy as nm
from irm.swapmarket import SwaptionMarket
from scipy.linalg import eigh, cholesky
from scipy.stats import norm


def writeToCSV(obj, fullyQualifiedFileName):
    nm.savetxt(fullyQualifiedFileName, obj, delimiter=",")

'''returns an immutable list (tuple) where the first value is the thetaD and the second is thetaD''' 
def getCMSSwapletConvexivityCorrectionParams(s0, fCMS, f, n):
    thetaD = (s0/fCMS) / (1 + (s0/f))
    t1 = (s0/f)/(1 + (s0/f))
    t2 = f/fCMS
    t3 = n/(pow(1 + s0/f, n) -1)
    thetas = 1 - t1 * (t2 + t3)
    return (thetas + thetaD, thetaD)
    
if __name__ == '__main__':
    epsilon = 10**-8
    
    ''''Build a swap market out of the excel'''
    maturities = nm.matrix([0.25, .5, 1, 2, 3, 5]).T
    forwardSwapRates = nm.matrix([3.510, 3.632, 3.872, 4.313, 4.657, 5.03]).T
    moneyness = nm.matrix([-250, -200, -150, -100, -50, -25, 0.00, 25, 50, 100, 150, 200, 250]).T
    lognormalVols = nm.matrix([
                        [0.4868776044,0.4170073060,0.3758977768,0.3534962835,0.3442718610,0.3431218514,0.3435808776,0.3452180973,0.3476887415,0.3541405025,0.3615503235,0.3691991111,0.3767322609],
                        [0.4539760008,0.3974819584,0.3628905988,0.3425116600,0.3320458504,0.3294890133,0.3282494355,0.3280359594,0.3286064531,0.3313575252,0.3353904350,0.3400453399,0.3449439178],
                        [0.4110175023,0.3671137865,0.3383383319,0.3199176401,0.3090205765,0.3057064352,0.3035022285,0.3022050613,0.3016387697,0.3021258197,0.3040531866,0.3068231897,0.3100573459],
                        [0.3630794746,0.3304871753,0.3073736657,0.2914311515,0.2811245681,0.2776788688,0.2751713369,0.2734625227,0.2724241891,0.2719128267,0.2728863761,0.2748056862,0.2773026544],
                        [0.3304251775,0.3026288522,0.2817582675,0.2665020066,0.2559626471,0.2522089185,0.2493290459,0.2472190641,0.2457767005,0.2445116757,0.2448499416,0.2462590545,0.2483537510],
                        [0.2969228412,0.2733096733,0.2545090655,0.2398825425,0.2290237602,0.2248891967,0.2215447128,0.2189261397,0.2169629323,0.2147045879,0.2141850553,0.2148909218,0.2164188674]
                        ])
    
    '''
    =====================================================================
    Code inside the SwaptionMarket performs HW3 #2,#3,#4 
    ======================================================================
    '''
    
    sm = SwaptionMarket(maturities, moneyness, forwardSwapRates, lognormalVols, 0.5)
    
    print('\nT = ')
    print(maturities[2,0])
    
    print('\n[sigma0, alpha, beta, rho] =')
    params = nm.append(sm.sigma0,sm.alpha,axis=1)
    params = nm.append(params,sm.beta,axis=1)
    params = nm.append(params,sm.rho,axis=1)
    print(params[2,:])
    
    print('\n1x10 ATM SABR Delta = ' + str(-of.sabr_normal_put_delta(1, 0.01*forwardSwapRates[2], 0.01*forwardSwapRates[2], sm.sigma0[2], sm.alpha[2], sm.beta[2], sm.rho[2], maturities[2])))
    
    print('\n1x10 ATM Black-Scholes Delta = ' + str(-of.blacks_put_delta(1, 0.01*forwardSwapRates[2], 0.01*forwardSwapRates[2], lognormalVols[2,6], maturities[2])))    
    
    oneByTenSwapRate = 1e-2 *  3.872
    oneByTenVol = 1e-2 * 30.35

    fiveYearSwapRate = 1e-2 * 5.030
    fiveYearvol = 1e-2 * 22.15 
    
    fCMS = 4.00
    f = 2.00
    n = 20.00
    
    '''Doing the 1 x 10 swap calculations'''
    thetas = getCMSSwapletConvexivityCorrectionParams(oneByTenSwapRate, fCMS, f, n)
    thetaTotal = thetas[0] - thetas[1]
    print('\nthetas 1 * 10 year = ' + str(thetas))
    print('Theta 1 * 10 delay fraction = ' + str((-thetas[1] / thetaTotal)))
    
    totalCorrection = oneByTenSwapRate * thetaTotal * nm.exp((pow(oneByTenVol, 2) * 1) - 1)
    print('1 x 10 CMS Rate = ' + str(oneByTenSwapRate + totalCorrection))
    
    '''Doing the 5 x 10 swap calculations'''
    fiveByTenSwapRate = 1e-2 * 5.030
    fiveByTenvol = 1e-2 * 22.15 
    thetas = getCMSSwapletConvexivityCorrectionParams(fiveByTenSwapRate, fCMS, f, n)
    thetaTotal = thetas[0] - thetas[1]
    print('thetas 5 * 10 year = ' + str(thetas))
    print('Theta 5 * 10 delay fraction = ' + str((-thetas[1] / thetaTotal)))
    
    totalCorrection = fiveByTenSwapRate * thetaTotal * nm.exp((pow(oneByTenVol, 2) * 1) - 1)
    print('5 x 10 CMS Rate = ' + str(oneByTenSwapRate + totalCorrection))