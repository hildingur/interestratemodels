'''
Created on Sep 16, 2013

@author: hildingur
'''

import irm.splines
import irm.rate_funcs as rates
import numpy as nm
import matplotlib.pyplot as plt
from scipy.optimize import root
import itertools


if __name__ == '__main__':
   knots = nm.matrix([[-4, -3, -2, -1,  0,  1,  2,  5,  7,  10, 11, 12, 13, 14]])
   spline = irm.splines.SplineBasis(knots,3)
   maxT = 10
   libor_rates = nm.matrix([[ 0.05,  0.05,  0.05,  0.05,  0.05]]).T
   libor_S = nm.matrix([[ 0.  ,  0.25,  0.5 ,  0.75,  1.  ]])
   libor_T = nm.matrix([[ 0.25,  0.5 ,  0.75,  1.  ,  1.25]])
   libor_daycnt_frac = nm.matrix([[ 0.25,  0.25,  0.25,  0.25,  0.25]])
   swap_rates = nm.matrix([[ 0.05,  0.05,  0.05,  0.05,  0.05]]).T
   swap_tenors = nm.matrix([[ 2,  3,  5,  7, 10]])
   swap_T_fixed = nm.matrix([[  0.5,   1. ,   1.5,   2. ,   2.5,   3. ,   3.5,   4. ,   4.5,
           5. ,   5.5,   6. ,   6.5,   7. ,   7.5,   8. ,   8.5,   9. ,
           9.5,  10. ]])
   swap_S_float = nm.matrix([[ 0.  ,  0.25,  0.5 ,  0.75,  1.  ,  1.25,  1.5 ,  1.75,  2.  ,
          2.25,  2.5 ,  2.75,  3.  ,  3.25,  3.5 ,  3.75,  4.  ,  4.25,
          4.5 ,  4.75,  5.  ,  5.25,  5.5 ,  5.75,  6.  ,  6.25,  6.5 ,
          6.75,  7.  ,  7.25,  7.5 ,  7.75,  8.  ,  8.25,  8.5 ,  8.75,
          9.  ,  9.25,  9.5 ,  9.75]])
   swap_T_float = nm.matrix([[  0.25,   0.5 ,   0.75,   1.  ,   1.25,   1.5 ,   1.75,   2.  ,
           2.25,   2.5 ,   2.75,   3.  ,   3.25,   3.5 ,   3.75,   4.  ,
           4.25,   4.5 ,   4.75,   5.  ,   5.25,   5.5 ,   5.75,   6.  ,
           6.25,   6.5 ,   6.75,   7.  ,   7.25,   7.5 ,   7.75,   8.  ,
           8.25,   8.5 ,   8.75,   9.  ,   9.25,   9.5 ,   9.75,  10.  ]])
   fedfunds_rate = 0.05
   basis_spreads = nm.matrix([[0, 0, 0, 0, 0]]).T
   basis_swap_tenors_quarters = nm.matrix([[ 8, 12, 20, 28, 40]])
   basis_swap_S = nm.matrix([[ 0.  ,  0.25,  0.5 ,  0.75,  1.  ,  1.25,  1.5 ,  1.75,  2.  ,
          2.25,  2.5 ,  2.75,  3.  ,  3.25,  3.5 ,  3.75,  4.  ,  4.25,
          4.5 ,  4.75,  5.  ,  5.25,  5.5 ,  5.75,  6.  ,  6.25,  6.5 ,
          6.75,  7.  ,  7.25,  7.5 ,  7.75,  8.  ,  8.25,  8.5 ,  8.75,
          9.  ,  9.25,  9.5 ,  9.75]])
   basis_swap_T = nm.matrix([[  0.25,   0.5 ,   0.75,   1.  ,   1.25,   1.5 ,   1.75,   2.  ,
           2.25,   2.5 ,   2.75,   3.  ,   3.25,   3.5 ,   3.75,   4.  ,
           4.25,   4.5 ,   4.75,   5.  ,   5.25,   5.5 ,   5.75,   6.  ,
           6.25,   6.5 ,   6.75,   7.  ,   7.25,   7.5 ,   7.75,   8.  ,
           8.25,   8.5 ,   8.75,   9.  ,   9.25,   9.5 ,   9.75,  10.  ]])
   basis_swap_daycnt_frac = nm.matrix([[ 0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,
          0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,
          0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,
          0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,  0.25,
          0.25,  0.25,  0.25,  0.25]])
          
   myrates = rates.Rates(spline, spline, maxT, libor_rates, libor_S, libor_T, libor_daycnt_frac, swap_rates, swap_tenors, swap_T_fixed, swap_S_float, swap_T_float, fedfunds_rate, basis_spreads, basis_swap_tenors_quarters, basis_swap_S, basis_swap_T, basis_swap_daycnt_frac)
   
   ois_w = nm.transpose(nm.matrix([[ 0.05,  0.05,  0.05,  0.05,  0.05,  0.05,  0.05,  0.05]]))
   libor_w = ois_w
   
   print(myrates.err(0,ois_w,libor_w))
   print(myrates.jacob(0,ois_w,libor_w))
   
   lam = 0
   
   def obj(weights):
       ois_w = nm.matrix(weights[0:int(len(weights)/2)]).T
       libor_w = nm.matrix(weights[int(len(weights)/2):len(weights)]).T
       return myrates.err(lam,ois_w,libor_w).tolist()[0]
          
   def objJac(weights):
       ois_w = nm.matrix(weights[0:int(len(weights)/2)]).T
       libor_w = nm.matrix(weights[int(len(weights)/2):len(weights)]).T
       return myrates.err(lam,ois_w,libor_w).tolist()[0], nm.array(myrates.jacob(lam,ois_w,libor_w))
   
   
   print("optimization")
   sol = root(obj, list(itertools.repeat(0,2*(knots.shape[1]-6))), jac=False, method="lm")
   #sol = root(objJac, list(itertools.repeat(0,2*(knots.shape[1]-6))), jac=True, method="lm")
   print(sol)
   print(sol.x)
   
   print("err(x)")
   print(obj(list(sol.x)))
   print("guess weight err")
   print(myrates.err(0,ois_w,libor_w))
   
   
