import numpy as nm
from irm.option_funcs import logn2normIV
from irm.option_funcs import sabrNormIV
from scipy.optimize import root

class SwaptionMarket:
	def __init__(self, maturations, moneyness, fowradSwapRates, lognormalVols, beta):
		self.maturations = maturations
		self.moneyness = moneyness
		self.forwardSwapRates = fowradSwapRates
		self.lognormalVols = lognormalVols
		
		self.normalVols = nm.matrix(self.lognormalVols)
		
		for c in range(0, self.lognormalVols.shape[1]):
			money = self.moneyness[c, 0]
			for r in range(0, self.lognormalVols.shape[0]):
				t = self.maturations[r, 0]
				forward = self.forwardSwapRates[r, 0]
				sigLogn = self.lognormalVols[r, c]
				k = forward/100 + money/10000
				f = forward/100
				self.normalVols[r, c] = logn2normIV(f, k, sigLogn, t)
				
		self.calibrateSABR(beta)
				
	def sabrNormVols(self, sigma0, alpha, beta, rho, r):
		
		sabrVols = nm.matrix(self.normalVols[r,:]).T
		
		t = self.maturations[r, 0]
		forward = self.forwardSwapRates[r, 0]
		f = forward/100
		
		for c in range(0, self.lognormalVols.shape[1]):
			money = self.moneyness[c, 0]
			k = forward/100 + money/10000
			sabrVols[c,0] = sabrNormIV(alpha, beta, rho, f, k, sigma0, t)
				
		return sabrVols
	
	def sabrErr(self, sigma0, alpha, beta, rhoTransformed, r):
		
		rho = (1-nm.exp(rhoTransformed))/(1+nm.exp(rhoTransformed))
		return (self.normalVols[r,:].T - self.sabrNormVols(sigma0, alpha, beta, rho, r)).T.tolist()[0]
	
	def sabrErrWrap(self, params, beta, r): return self.sabrErr(params[0], params[1], beta, params[2], r)
	
	def calibrateSABR(self, beta):
		
		nrows = self.lognormalVols.shape[0]
		self.sigma0 = nm.zeros((nrows,1))
		self.alpha = nm.zeros((nrows,1))
		self.beta = nm.zeros((nrows,1))
		self.rho = nm.zeros((nrows,1))
		
		for r in range(0, nrows):
			params = root(self.sabrErrWrap, x0=[0.1,0.3,0.0], args=(beta,r), method="lm").x
			self.sigma0[r,0] = params[0]
			self.alpha[r,0] = params[1]
			self.rho[r,0] = (1-nm.exp(params[2]))/(1+nm.exp(params[2]))
			self.beta[r,0] = beta
			if(self.alpha[r,0] < 0):
				self.alpha[r,0] = -self.alpha
				self.rho[r,0] = -self.rho
	
	