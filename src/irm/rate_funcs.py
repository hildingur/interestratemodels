
import numpy as nm

class InstantForward(object):
	
	def __init__(self, bspline, w):
		
		self.bspline = bspline
		self.w = w
		
	def value(self, t):
		
		val = self.bspline.get_basis_value(t[0,0])
		for i in range(1, t.shape[0]):
			val = nm.append(val, self.bspline.get_basis_value(t[i,0]), axis=0)
			
		return val[:,1:(1+self.w.shape[0])]*self.w


class DiscountCurve(object):

	def __init__(self, bspline, T):
	
		self.gamma = bspline.integrate(3, 0, T[0, 0])
		for i in range(1, T.shape[1]):
			self.gamma = nm.append(self.gamma, bspline.integrate(3, 0, T[0, i]), axis=0)
		self.gamma = self.gamma[:,range(1,self.gamma.shape[1] - 5)]
		
	def disc_factors(self, w): return nm.exp(-self.gamma*w)
	
	def jacob(self, w): return nm.multiply(self.gamma, -nm.exp(-self.gamma*w))
	
	
class Annuity(object):
	
	def __init__(self, bspline, ois_w, T_max):
		
		self.disc_curve = DiscountCurve(bspline, 0.25*nm.matrix(range(1,4*T_max)))
		self.disc_factors = self.disc_curve.disc_factors(ois_w)
		
	def annuity(self, S, T):
		
		value = 0
		i_s = int(nm.round(4*S))
		for i in range(0, int(nm.round(T - S))):
			value = value + self.disc_factors[i_s + 4*i, 0] + self.disc_factors[i_s + 4*i + 2, 0]
		return 0.5*value


class LiborCurve(object):

	def __init__(self, bspline, S, T, daycnt_frac):
	
		self.gamma = bspline.integrate(3, S[0, 0], T[0, 0])
		for i in range(1, T.shape[1]):
			self.gamma = nm.append(self.gamma, bspline.integrate(3, S[0, i], T[0, i]), axis=0)
		self.gamma = self.gamma[:,range(1,self.gamma.shape[1] - 5)]
		
		self.daycnt_frac = nm.transpose(daycnt_frac)
		
	def forwards(self, w): return (nm.exp(self.gamma*w) - 1)/self.daycnt_frac
	
	def jacob(self, w): return nm.multiply(self.gamma, nm.exp(self.gamma*w)/self.daycnt_frac)


class SwapCurve(object):

	def __init__(self, ois_bspline, libor_bspline, tenors, T_fixed, S_float, T_float):
	
		self.disc_curve_fixed = DiscountCurve(ois_bspline, T_fixed)
		self.disc_curve_float = DiscountCurve(ois_bspline, T_float)
		self.libor_curve = LiborCurve(libor_bspline, S_float,  T_float, 1)
		
		self.payments_fixed = nm.zeros((tenors.shape[1], 2*tenors[0, tenors.shape[1] - 1]))
		for i in range(0, tenors.shape[1]):
			for j in range(0, 2*tenors[0, i]):
				self.payments_fixed[i, j] = 1
			
		self.payments_float = nm.zeros((tenors.shape[1], 4*tenors[0, tenors.shape[1] - 1]))
		for i in range(0, tenors.shape[1]):
			for j in range(0, 4*tenors[0, i]):
				self.payments_float[i, j] = 1
			
	def annuity(self, ois_w): return 0.5*self.payments_fixed*self.disc_curve_fixed.disc_factors(ois_w)
	
	def jacob_annuity(self, ois_w, libor_w): return nm.append(0.5*self.payments_fixed*self.disc_curve_fixed.jacob(ois_w), nm.zeros((self.payments_fixed.shape[0],libor_w.shape[0])), axis=1)
		
	def float_leg(self, ois_w, libor_w): return self.payments_float*nm.multiply(self.disc_curve_float.disc_factors(ois_w), self.libor_curve.forwards(libor_w))
	
	def jacob_float_leg(self, ois_w, libor_w): return self.payments_float*nm.append(nm.multiply(self.disc_curve_float.jacob(ois_w), self.libor_curve.forwards(libor_w)), nm.multiply(self.disc_curve_float.disc_factors(ois_w), self.libor_curve.jacob(libor_w)), axis=1)
	
	def swap_rates(self, ois_w, libor_w): return self.float_leg(ois_w, libor_w)/self.annuity(ois_w)
	
	def jacob_swap_rates(self, ois_w, libor_w): return nm.multiply(1/self.annuity(ois_w), self.jacob_float_leg(ois_w, libor_w)) - nm.multiply(self.float_leg(ois_w, libor_w), self.jacob_annuity(ois_w, libor_w))


class FedFunds(object):

	def __init__(self, ois_bspline):
	
		self.gamma = ois_bspline.integrate(3, 0, 1/365)
		self.gamma = self.gamma[:,range(1,self.gamma.shape[1] - 5)]
		
	def rate(self, w): return 365*(nm.exp(self.gamma*w) - 1)
	
	def jacob(self, w): return nm.multiply(self.gamma, 365*nm.exp(self.gamma*w))


class BasisSwapCurve(object):

	def __init__(self, ois_bspline, libor_bspline, tenors_quarters, S, T, daycnt_frac):
	
		self.disc_curve = DiscountCurve(ois_bspline, T)
		self.ois_curve = LiborCurve(ois_bspline, S, T, 1)
		self.libor_curve = LiborCurve(libor_bspline, S, T, 1)
		
		self.payments_float = nm.zeros((tenors_quarters.shape[1], tenors_quarters[0, tenors_quarters.shape[1] - 1]))
		for i in range(0, tenors_quarters.shape[1]):
			for j in range(0, tenors_quarters[0, i]):
				self.payments_float[i, j] = 1
		
		self.payments_fixed = nm.multiply(daycnt_frac, self.payments_float)
	
	def annuity(self, ois_w): return self.payments_fixed*self.disc_curve.disc_factors(ois_w)
	
	def jacob_annuity(self, ois_w, libor_w): return nm.append(self.payments_fixed*self.disc_curve.jacob(ois_w), nm.zeros((self.payments_fixed.shape[0],libor_w.shape[0])), axis=1)
	
	def float_leg(self, ois_w, libor_w): return self.payments_float*nm.multiply(self.disc_curve.disc_factors(ois_w), self.libor_curve.forwards(libor_w) - self.ois_curve.forwards(ois_w))
	
	def jacob_float_leg(self, ois_w, libor_w): return self.payments_float*nm.append(nm.multiply(self.disc_curve.jacob(ois_w), self.libor_curve.forwards(libor_w) - self.ois_curve.forwards(ois_w)) + nm.multiply(self.disc_curve.disc_factors(ois_w), -self.ois_curve.jacob(ois_w)), nm.multiply(self.disc_curve.disc_factors(ois_w), self.libor_curve.jacob(libor_w)), axis=1)
	
	def spreads(self, ois_w, libor_w): return self.float_leg(ois_w, libor_w)/self.annuity(ois_w)
	
	def jacob_spreads(self, ois_w, libor_w): return nm.multiply(1/self.annuity(ois_w), self.jacob_float_leg(ois_w, libor_w)) - nm.multiply(self.float_leg(ois_w, libor_w), self.jacob_annuity(ois_w, libor_w))


class Rates(object):

	def __init__(self, ois_bspline, libor_bspline, maxT, libor_rates, libor_S, libor_T, libor_daycnt_frac, swap_rates, swap_tenors, swap_T_fixed, swap_S_float, swap_T_float, fedfunds_rate, basis_spreads, basis_swap_tenors_quarters, basis_swap_S, basis_swap_T, basis_swap_daycnt_frac):
	
		self.libor_rates = libor_rates
		self.libor = LiborCurve(libor_bspline, libor_S, libor_T, libor_daycnt_frac)
		self.swap_rates = swap_rates
		self.swap = SwapCurve(ois_bspline, libor_bspline, swap_tenors, swap_T_fixed, swap_S_float, swap_T_float)
		self.fedfunds_rate = fedfunds_rate
		self.fedfunds = FedFunds(ois_bspline)
		self.basis_spreads = basis_spreads
		self.basis = BasisSwapCurve(ois_bspline, libor_bspline, basis_swap_tenors_quarters, basis_swap_S, basis_swap_T, basis_swap_daycnt_frac)
		self.penalty_ois = ois_bspline.penalty(0, maxT)[range(1,1+self.libor.gamma.shape[1])]
		self.penalty_ois = self.penalty_ois[:,range(1,1+self.libor.gamma.shape[1])]
		self.penalty_libor = libor_bspline.penalty(0, maxT)[range(1,1+self.libor.gamma.shape[1])]
		self.penalty_libor = self.penalty_libor[:,range(1,1+self.libor.gamma.shape[1])]
		
	def err(self, lam, ois_w, libor_w):
	
		e = nm.append(self.libor.forwards(libor_w) - self.libor_rates, self.swap.swap_rates(ois_w, libor_w) - self.swap_rates, axis=0)
		e = nm.append(e, 100*(self.fedfunds.rate(ois_w) - self.fedfunds_rate), axis=0)
		e = nm.append(e, self.basis.spreads(ois_w, libor_w) - self.basis_spreads, axis=0)
		e = nm.append(e, nm.matrix(nm.sqrt(lam*(nm.transpose(ois_w)*self.penalty_ois*ois_w + nm.transpose(libor_w)*self.penalty_libor*libor_w))), axis=0)
		return nm.transpose(e)
		
	def jacob(self, lam, ois_w, libor_w):
	
		j = nm.append(nm.append(nm.zeros((self.libor_rates.shape[0], ois_w.shape[0])), self.libor.jacob(libor_w), axis=1), self.swap.jacob_swap_rates(ois_w, libor_w), axis=0)
		j = nm.append(j, nm.append(self.fedfunds.jacob(ois_w), nm.zeros((1, libor_w.shape[0])), axis=1), axis=0)
		j = nm.append(j, self.basis.jacob_spreads(ois_w, libor_w), axis=0)
		j = nm.append(j, nm.append(nm.sqrt(lam/(nm.transpose(ois_w)*self.penalty_ois*ois_w))*nm.transpose(ois_w)*self.penalty_ois, nm.sqrt(lam/(nm.transpose(libor_w)*self.penalty_libor*libor_w))*nm.transpose(libor_w)*self.penalty_libor, axis=1), axis=0)
		return j
		
class CalibrationResults(object):
	def __init__(self, ois_bspline, libor_bspline, rates, ois_weights, libor_weights):
		self.ois_bspline = ois_bspline
		self.libor_bspline = libor_bspline
		self.rates = rates
		self.ois_weights = ois_weights
		self.libor_weights = libor_weights


	