from scipy.stats import norm
import numpy as nm
import scipy.optimize as optim

def N(x):
    return norm.cdf(x)

def Nd(x):
    return nm.sqrt(1.00 / (2.00 * nm.pi)) * nm.exp(-1.00 * (x**2) / 2.00)

def blacks_dplus(f0, k, sig, T):
    return (nm.log(f0/k) + 0.5 * (sig**2) * T) / (sig * nm.sqrt(T))

def blacks_dminus(f0, k, sig, T):
    return (nm.log(f0/k) - 0.5 * (sig**2) * T) / (sig * nm.sqrt(T))
    
def blacks_call(n0, f0, k, sig, T):
    return n0 * ((f0 * N(blacks_dplus(f0, k, sig, T))) - (k * N(blacks_dminus(f0, k, sig, T))))

def blacks_put(n0, f0, k, sig, T):
    return n0 * ((-1.00 * f0 * N(-1.00 * blacks_dplus(f0, k, sig, T))) + (k * N(-1.00 * blacks_dminus(f0, k, sig, T))))

def blacks_call_delta(n0, f0, k, sig, T):
    return 5000*(blacks_call(n0,f0+0.0001,k,sig,T) - blacks_call(n0,f0-0.0001,k,sig,T))

def blacks_put_delta(n0, f0, k, sig, T):
    return 5000*(blacks_put(n0,f0+0.0001,k,sig,T) - blacks_put(n0,f0-0.0001,k,sig,T))

def normal_dplus(f0, k, sig, T):
    return (f0 - k) / (sig * nm.sqrt(T))

def normal_dminus(f0, k, sig, T):
    return -1.00 * normal_dplus(f0, k, sig, T)

def normal_call(n0, f0, k, sig, T):
    dp = normal_dplus(f0, k, sig, T)
    return n0 * sig * nm.sqrt(T) * (dp * N(dp) + Nd(dp)) 

def normal_put(n0, f0, k, sig, T):
    dm = normal_dminus(f0, k, sig, T)
    return n0 * sig * nm.sqrt(T) * (dm * N(dm) + Nd(dm))

def zeroLognMinusNorm(sigNorm, f0, k, sigLogn, T): return blacks_call(1, f0, k, sigLogn, T) - normal_call(1, f0, k, sigNorm, T)

def logn2normIV(f0, k, sigLogn, T): return optim.newton(zeroLognMinusNorm, sigLogn*k, args=(f0,k,sigLogn,T), tol=1e-6, maxiter=10000)

def sabrNormIV(alpha, beta, rho, f0, k, sigma0, T):
    
    zeta = (alpha/(sigma0*(1-beta)))*(nm.power(f0,1-beta)-nm.power(k,1-beta))
    delta = nm.log((nm.sqrt(1-2*rho*zeta+zeta*zeta)+zeta-rho)/(1-rho))
    eps = alpha*alpha*T
    fmid = 0.5*(f0+k)
    c = nm.power(fmid,beta)
    gamma1 = beta/fmid
    gamma2 = -beta*(1-beta)/(fmid*fmid)
    
    if(nm.abs(f0-k)<1e-6):
        sigmaN = sigma0*nm.power(f0,beta)*(1+eps*(-(2*beta-beta*beta)/24*nm.power(sigma0/(alpha*nm.power(f0,1-beta)),2) + rho*beta/4*sigma0/(alpha*nm.power(f0,1-beta)) + (2-3*rho*rho)/24))
    else: 
        sigmaN = alpha*(f0-k)/delta*(1+eps*((2*gamma2-gamma1*gamma1)/24*nm.power(sigma0*c/alpha,2) + rho*gamma1/4*sigma0*c/alpha + (2-3*rho*rho)/24))
    return sigmaN

def sabr_normal_call(n0, f0, k, sig0, alpha, beta, rho, T):
    return normal_call(n0=n0, f0=f0, k=k, sig=sabrNormIV(alpha=alpha, beta=beta, rho=rho, f0=f0, k=k, sigma0=sig0, T=T), T=T)

def sabr_normal_put(n0, f0, k, sig0, alpha, beta, rho, T):
    return normal_put(n0=n0, f0=f0, k=k, sig=sabrNormIV(alpha=alpha, beta=beta, rho=rho, f0=f0, k=k, sigma0=sig0, T=T), T=T)

def sabr_normal_call_delta(n0, f0, k, sig0, alpha, beta, rho, T):
    return 5000*(sabr_normal_call(n0,f0+0.0001,k,sig0,alpha,beta,rho,T) - sabr_normal_call(n0,f0-0.0001,k,sig0,alpha,beta,rho,T))

def sabr_normal_put_delta(n0, f0, k, sig0, alpha, beta, rho, T):
    return 5000*(sabr_normal_put(n0,f0+0.0001,k,sig0,alpha,beta,rho,T) - sabr_normal_put(n0,f0-0.0001,k,sig0,alpha,beta,rho,T))
