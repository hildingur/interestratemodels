'''
Created on Sep 15, 2013

@author: hildingur
'''

import numpy as nm
import itertools 

class SplineBasis(object):
    '''
    irm.Splines.SplineBasis - a class that encapsulates the SplineBasis
    '''

    def __init__(self, knot_points, d):
        '''
        knot_points - a list of knot points around which the interpolation will be based
        d - the degree of the interpolation, for eg 3 would be a cubic interpolation
        '''
        
        if(not isinstance(knot_points, nm.matrix)):
            raise Exception("The knot_points variable needs to be of type matrix")
        if(knot_points.shape[1] <= (d + 2)):
            msg = "There must be at-least " + str(d + 3) + \
                            " knot points to create an interpolation of degree " + str(d)
            raise Exception(msg)
        self.knot_points = knot_points
        self.knot_points.sort()
        self.degree = d
        self.__basis_cache = dict() #Contains the calculation cache
        self.__basis_first_derivative_cache = dict()
        self.__basis_second_derivative_cache = dict()
        self.__basis_third_derivative_cache = dict()
        self.__tolerance = 10**-6 #Mathematical tolerance set eo 10e-6
    
    def __get_current_k(self, t):
        knot_points_count = self.knot_points.shape[1]
        current_k = 0
        for k in range(0, knot_points_count - 1):
            if(t >= self.knot_points.item(0, k) and t < self.knot_points.item(0, k + 1)):
                current_k = k
                break;
        return current_k
            
        
    def __calculate_basis_value_for_degree(self, t, d):
        #Looking for the k value of the interval
        
        cache_key = (d, t)
        if(cache_key in self.__basis_cache.keys()):
            return self.__basis_cache[cache_key]
        
        knot_points_count = self.knot_points.shape[1]
        current_k = self.__get_current_k(t)
        
        basis_at_degree = nm.matrix(list(itertools.repeat(float(0), knot_points_count)))
        if(d==0): #For the zero'th basis, it's just an indicator function
            basis_at_degree.put(current_k, 1)
        else:
            #basis_at_last_degree_key = (d-1, t)
            #if(basis_at_last_degree_key not in self.__basis_cache.keys()):
            #    raise RuntimeError("Unable to find the basis for degree " + \
            #                      (d - 1) + " when trying to calculate the basis for degree " + d)
            #basis_at_last_degree = self.__basis_cache[basis_at_last_degree_key]
            basis_at_last_degree = self.__calculate_basis_value_for_degree(t, d-1)
            #Variables used in the equations
            for k in range(0, (knot_points_count - d - 1)):
                t_k = self.knot_points.item(0, k)
                t_k_plus_d = self.knot_points.item(0, (k + d))
                b_k_d_minus_one = basis_at_last_degree.item(0, k)
                t_k_plus_d_plus_one = self.knot_points.item(0, (k + d + 1))
                t_k_plus_one = self.knot_points.item(0, k + 1)
                b_k_plus_one_d_minus_one = basis_at_last_degree.item(0, k + 1)
                value = (((t - t_k) / (t_k_plus_d - t_k)) * b_k_d_minus_one) + \
                        (((t_k_plus_d_plus_one - t) / (t_k_plus_d_plus_one - t_k_plus_one)) * b_k_plus_one_d_minus_one)
                basis_at_degree.put(k, value)
                
        if(abs(basis_at_degree.sum(1) - 1) > self.__tolerance):
            raise Exception("The caluclation at degree " + str(d) + \
                            " got corrupted. The most common reason for this is that" + \
                            " your knot_points did not have enough (extra) points" + \
                            " at the start and the" + \
                            " end of the array")
        
        self.__basis_cache[cache_key] = basis_at_degree
        
        return basis_at_degree
    
        
    def get_basis_value(self, t):
        '''Returns a list of spline weights at t'''
        """
        length = self.knot_points.shape[1]
        
        if(t < self.knot_points.item(0, 0) or t > self.knot_points.item(0, length - 1)):
            raise Exception("Attempting to interpolate a point outside the range of the knot points")

        cache_key = (self.degree, t)
        if(cache_key not in self.__basis_cache.keys()):
            #self.__basis_cache[cache_key] = self.__calculate_basis_value(t)
            for d in range(0, (self.degree + 1)):
                cache_key_d = (d, t)
                if(cache_key_d not in self.__basis_cache.keys()):
                    self.__basis_cache[cache_key_d] = self.__calculate_basis_value_for_degree(t, d)
    
        return self.__basis_cache[cache_key]
        """
        return self.__calculate_basis_value_for_degree(t, self.degree)
    
    def get_basis_value_d1_for_degree(self, t, d):
        
        get_basis_value_d1_key = (d, t)
        if get_basis_value_d1_key in self.__basis_first_derivative_cache.keys():
            return self.__basis_first_derivative_cache[get_basis_value_d1_key]
        
        knot_points_count = self.knot_points.shape[1]
        #basis_at_last_degree = self.__basis_cache[(d - 1, t)]
        basis_at_last_degree = self.__calculate_basis_value_for_degree(t, d - 1)
        
        d1 = nm.matrix(list(itertools.repeat(float(0), knot_points_count)))
        for k in range(0, (knot_points_count - self.degree - 1)):
            t_k = self.knot_points.item(0, k)
            t_k_plus_d = self.knot_points.item(0, (k + d))
            b_k_d_minus_one = basis_at_last_degree.item(0, k)
            t_k_plus_d_plus_one = self.knot_points.item(0, (k + d + 1))
            b_k_plus_one_d_minus_one = basis_at_last_degree.item(0, k + 1)
            t_k_plus_one = self.knot_points.item(0, k + 1)
            val = ((d/(t_k_plus_d - t_k)) * b_k_d_minus_one) \
                - ((d/(t_k_plus_d_plus_one - t_k_plus_one)) * b_k_plus_one_d_minus_one)
            d1.put(k, val)
        
        self.__basis_first_derivative_cache[get_basis_value_d1_key] = d1
        return d1
    
    def get_basis_value_d2_for_degree(self, t, d):
        
        knot_points_count = self.knot_points.shape[1]
        
        get_basis_value_d2_key = (d, t)
        if get_basis_value_d2_key in self.__basis_second_derivative_cache.keys():
            return self.__basis_second_derivative_cache[get_basis_value_d2_key]
            
        #basis_first_derivative_key = (d - 1, t)
        #if basis_first_derivative_key not in self.__basis_first_derivative_cache.keys():
        #    self.__basis_first_derivative_cache[basis_first_derivative_key] = self.__get_basis_value_d1_for_degree(t, d - 1)
            
        basis_first_derivative = self.get_basis_value_d1_for_degree(t, d - 1)
        
        d2 = nm.matrix(list(itertools.repeat(float(0), knot_points_count)))
        for k in range(0, (knot_points_count - self.degree - 1)):
            t_k = self.knot_points.item(0, k)
            t_k_plus_d = self.knot_points.item(0, (k + d))
            b_k_d_minus_one = basis_first_derivative.item(0, k)
            t_k_plus_d_plus_one = self.knot_points.item(0, (k + d + 1))
            b_k_plus_one_d_minus_one = basis_first_derivative.item(0, k + 1)
            t_k_plus_one = self.knot_points.item(0, k + 1)
            val = ((d/(t_k_plus_d - t_k)) * b_k_d_minus_one) \
                - ((d/(t_k_plus_d_plus_one - t_k_plus_one)) * b_k_plus_one_d_minus_one)
            d2.put(k, val)
        
        self.__basis_second_derivative_cache[get_basis_value_d2_key] = d2
        return d2
    
    
    def get_basis_value_d3_for_degree_old(self, t, d):
       
        knot_points_count = self.knot_points.shape[1]
        
        get_basis_value_d3_key = (d, t)
        if get_basis_value_d3_key in self.__basis_third_derivative_cache.keys():
            return self.__basis_third_derivative_cache[get_basis_value_d3_key]
        
        #basis_second_derivative = self.get_basis_value_d2_for_degree(t, d)
        basis_second_derivative = self.get_basis_value_d2_for_degree(t, d)
        
        d3 = nm.matrix(list(itertools.repeat(float(0), knot_points_count)))
        for k in range(0, (knot_points_count - self.degree - 1)):
            t_k = self.knot_points.item(0, k)
            t_k_plus_d = self.knot_points.item(0, (k + d))
            b_k_d_minus_one = basis_second_derivative.item(0, k)
            t_k_plus_d_plus_one = self.knot_points.item(0, (k + d + 1))
            b_k_plus_one_d_minus_one = basis_second_derivative.item(0, k + 1)
            t_k_plus_one = self.knot_points.item(0, k + 1)
            val = ((d/(t_k_plus_d - t_k)) * b_k_d_minus_one) \
                - ((d/(t_k_plus_d_plus_one - t_k_plus_one)) * b_k_plus_one_d_minus_one)
            d3.put(k, val)
        
        self.__basis_third_derivative_cache[get_basis_value_d3_key] = d3
        return d3
    
    def get_basis_value_d3_for_degree(self, t1, t2, d):
        basis_second_derivative_t1 = self.get_basis_value_d2_for_degree(t1, d)
        basis_second_derivative_t2 = self.get_basis_value_d2_for_degree(t2, d)
        
        return (basis_second_derivative_t2 - basis_second_derivative_t1) / (t2 - t1)
    
    
    def get_basis_value_as_string(self, t):
        nf = "{:10.4f} " #The number formatter
        sf = "%-12s " #The string formatter
        self.get_basis_value(t) #make sure the cache is populated.
        retval = "\n" + " t = " + str(t) + "\n\n"
        retval = retval + sf % "knot pts ->"
        length = self.knot_points.shape[1]
        for i in range(0, length):
            retval = retval + (nf.format(self.knot_points.item(0, i)))

        retval = retval + "\n"
        for d in range(0, self.degree + 1):
            retval = retval + (sf % ("d = " + str(d)))
            cache_key = (d, t)
            for i in range(0, self.__basis_cache[cache_key].shape[1]):
                retval = retval + (nf.format(self.__basis_cache[cache_key].item(0, i)))
            retval = retval + "\n"

        return retval

    def get_as_string(self, m):
        nf = "{:10.4f} " #The number formatter
        retval = "\n" 
        for i in range(0, m.shape[0]):
            for j in range(0, m.shape[1]):
                retval = retval + str(nf.format(m[i][j]))
            retval = retval + "\n"
        return retval
    
    def integrate_neg_inf_to_t(self, t, d):
        
        self.get_basis_value(t)
        
        length = self.knot_points.shape[1]
        retval = nm.matrix(list(itertools.repeat(float(0), length)))        
        for k in range(0, length - d - 1):
            coef = (self.knot_points.item(0, k + d + 1) \
                   - self.knot_points.item(0, k)) / (d + 1)
            basis_d_plus_one = self.__calculate_basis_value_for_degree(t, d + 1)
            total = float(0);
            for i in range(k, basis_d_plus_one.shape[1]):
                total = total + basis_d_plus_one.item(i)
            
            retval.put(k, total * coef)
        return retval
    
    def integrate(self, d, a, b):
        return self.integrate_neg_inf_to_t(b, d) - self.integrate_neg_inf_to_t(a, d)
    
    def penalty(self, a, b):
        min = 0
        max = self.knot_points.shape[1] - self.degree - 1
        retval = nm.matrix(nm.zeros((max, max)))
        for k in range(min, max):
            for l in range(min, max):
                val = self.formula_34_k_l(k, l, a, b)
                retval.put((k * max) + l, val)
        
        return retval
    
    def formula_34_k_l(self, k, l, a, b):
        k_a = self.__get_current_k(a) + 1 #Interpreting this as being strictly between a, b
        k_b = self.__get_current_k(b) - 1
        d = self.degree
        
        last_total = float(0)
        
        for j in range(k_a, k_b+2):
            w_j_minus_one = self.knot_points.item(j - 1)
            w_j = self.knot_points.item(j)
            #self.get_basis_value_d3_for_degree(w_j, d).item(l): This is a hack.
            #We use the mid point in the 3rd derivative to work around the problem of disconintuity in the 3rd derviatives @ the KP's.
            #Since the 3rd derivative is piecewise constant at d=3, it should't matter
            
            last_total = last_total + \
                (self.get_basis_value_d3_for_degree(w_j_minus_one, w_j, d).item(l) * (self.get_basis_value(w_j).item(k) - self.get_basis_value(w_j_minus_one).item(k)))
        
        b_dash = (self.get_basis_value_d1_for_degree(b, d).item(k) * self.get_basis_value_d2_for_degree(b, d).item(l)) \
                - (self.get_basis_value_d1_for_degree(a, d).item(k) * self.get_basis_value_d2_for_degree(a, d).item(l)) \
                - last_total
        
        return b_dash
       
    def formula_34_k_l_old(self, k, l, a, b):
        k_a = self.__get_current_k(a) + 1 #Interpreting this as being strictly between a, b
        k_b = self.__get_current_k(b) - 1
        d = self.degree
        
        last_total = float(0)
        
        for j in range(k_a, k_b+2):
            w_j_minus_one = self.knot_points.item(j - 1)
            w_j = self.knot_points.item(j)
            #self.get_basis_value_d3_for_degree(w_j, d).item(l): This is a hack.
            #We use the mid point in the 3rd derivative to work around the problem of disconintuity in the 3rd derviatives @ the KP's.
            #Since the 3rd derivative is piecewise constant at d=3, it should't matter
            d1 = self.get_basis_value_d1_for_degree(w_j, 3)
            d2 = self.get_basis_value_d2_for_degree(w_j, 3)
            last_total = last_total + \
                (self.get_basis_value_d3_for_degree(w_j, d).item(l) * (self.get_basis_value(w_j).item(k) - self.get_basis_value(w_j_minus_one).item(k)))
        
        b_dash = (self.get_basis_value_d1_for_degree(b, d).item(k) * self.get_basis_value_d2_for_degree(b, d).item(l)) \
                - (self.get_basis_value_d1_for_degree(a, d).item(k) * self.get_basis_value_d2_for_degree(a, d).item(l)) \
                - last_total
        
        return b_dash
        
    def matrix_to_str(self, m):
        nf = "{:10.4f} "
        retval = ""
        for i in range(0, m.shape[0]):
            for j in range(0, m.shape[1]):
                retval = retval + nf.format(m[i, j])
            retval = retval + "\n"
        return retval
    
    
    def penalty_1(self, a, b):
        min = 0
        max = self.knot_points.shape[1] - self.degree - 1
        retval = nm.matrix(nm.zeros((max, max)))
        for k in range(min, max):
            for l in range(min, max):
                val = self.formula_34_k_l_1(k, l, a, b)
                retval.put((k * max) + l, val)
        
        return retval
       
    def formula_34_k_l_1(self, k, l, a, b):
        d = self.degree
        return (self.get_basis_value_d1_for_degree(b, d).item(k) * self.get_basis_value_d2_for_degree(b, d).item(l))
    
    def penalty_2(self, a, b):
        min = 0
        max = self.knot_points.shape[1] - self.degree - 1
        retval = nm.matrix(nm.zeros((max, max)))
        for k in range(min, max):
            for l in range(min, max):
                val = self.formula_34_k_l_2(k, l, a, b)
                retval.put((k * max) + l, val)
        
        return retval
       
    def formula_34_k_l_2(self, k, l, a, b):
        d = self.degree
        return -1.00 * (self.get_basis_value_d1_for_degree(a, d).item(k) * self.get_basis_value_d2_for_degree(a, d).item(l))
    
    def penalty_3(self, a, b):
        min = 0
        max = self.knot_points.shape[1] - self.degree - 1
        retval = nm.matrix(nm.zeros((max, max)))
        for k in range(min, max):
            for l in range(min, max):
                val = self.formula_34_k_l(k, l, a, b)
                retval.put((k * max) + l, val)
        
        return retval
       
    def formula_34_k_l_3(self, k, l, a, b):
        k_a = self.__get_current_k(a) + 1 #Interpreting this as being strictly between a, b
        k_b = self.__get_current_k(b) - 1
        d = self.degree
        
        last_total = float(0)
        
        for j in range(k_a, k_b+2):
            w_j_minus_one = self.knot_points.item(j - 1)
            w_j = self.knot_points.item(j)
            #self.get_basis_value_d3_for_degree(w_j, d).item(l): This is a hack.
            #We use the mid point in the 3rd derivative to work around the problem of disconintuity in the 3rd derviatives @ the KP's.
            #Since the 3rd derivative is piecewise constant at d=3, it should't matter
            
            last_total = last_total + \
                (self.get_basis_value_d3_for_degree(w_j, d).item(l) * (self.get_basis_value(w_j).item(k) - self.get_basis_value(w_j_minus_one).item(k)))
        
        return -1.00 * last_total
    